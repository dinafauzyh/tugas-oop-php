<?php  

    require_once("Animal.php");
    require_once("Frog.php");
    require_once("Ape.php");

    $sheep = new Animal("Shaun");
    echo "Name: " . $sheep->animalName . "<br>";
    echo "Legs: " . $sheep->legs . "<br>";
    echo "Cold Blooded: " . $sheep->cold_blooded . "<br><br>";

    $kodok = new Frog("Buduk");
    echo "Name: " . $kodok->animalName . "<br>";
    echo "Legs: " . $kodok->legs . "<br>";
    echo "Cold Blooded: " . $kodok->cold_blooded . "<br>";
    echo $kodok->jump();
    echo "<br><br>";

    $sungokong = new Ape("Kera Sakti");
    echo "Name: " . $sungokong->animalName . "<br>";
    echo "Legs: " . $sungokong->legs . "<br>";
    echo "Cold Blooded: " . $sungokong->cold_blooded . "<br>";
    echo $sungokong->yell();
    
?>